import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('juniper_virtual_chassis')


def test_virtual_chassis_exists(host):
    assert os.system("zcat /config/juniper.conf.gz | grep virtual-chassis")
