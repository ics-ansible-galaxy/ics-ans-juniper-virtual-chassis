# ics-ans-juniper-virtual-chassis

Playbook to autoconfigure switched switches.

Use case; Replace switch in a virtual chassi edit CS-Entry entry and run this playbook.

Runs as csi-network user and with netconf connection enabled. Run [ics-ans-juniper-netconf](https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-juniper-netconf) incase user and netconf not enabled.

## Role Variables

```yaml
juniper_juniper_virtual_chassis_user: false
juniper_virtual_chassis_password: user
juniper_virtual_chassis_password: "vaulted" Can be found in Bitwarden
```

## License

BSD 2-clause
